# REF SP11 Working Methods and Software

The REF 2021 Computer Science and Informatics sub-panel (SP) made a commitment to transparency in the processes and methods it used in assessing the submissions made to Unit of Assessment 11. This repository is intended to give the Computer Science & Informatics community, and the academic community more broadly, detailed information on the SP's working methods, including providing access to the software that was used to automate some aspects of the assessment.

The timetable for publishing material is as follows.

**1st June 2022**: Working Methods document, including an overview of software methods (now published)

**1st July 2022**: Python code for allocation and normalisation (now published)

We also hope to be able to publish REF 2021 data in anonymised form to allow testing and experimentation, but we do not yet have permission to do so. If we are unable to publish the real data, we will consider generating synthetic data, but we may simply give up the will to live!

If you have any questions or comments please contact:

Professor Tom Rodden, SP Chair: tom.rodden@nottingham.ac.uk 

Professor Chris Taylor, SP Deputy Chair: chris.taylor@manchester.ac.uk 
